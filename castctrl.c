#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <gtk/gtk.h>

/*
catt commands as of 20190328:
  add           Add a video to the queue.
  cast          Send a video to a Chromecast for playing.
  cast_site     Cast any website to a Chromecast.
  clear         Clear the queue.
  ffwd          Fastforward a video by TIME duration.
  info          Show complete information about the currently-playing video.
  pause         Pause a video.
  play          Resume a video after it has been paused.
  remove        Remove a video from the queue.
  restore       Return Chromecast to saved state.
  rewind        Rewind a video by TIME duration.
  save          Save the current state of the Chromecast for later use.
  scan          Scan the local network and show all Chromecasts and their IPs.
  seek          Seek the video to TIME position.
  skip          Skip to end of content.
  status        Show some information about the currently-playing video.
  stop          Stop playing.
  volume        Set the volume to LVL [0-100].
  volumedown    Turn down volume by a DELTA increment.
  volumeup      Turn up volume by a DELTA increment.
  write_config  Write the name of default Chromecast device to config file.
*/

#define STATUS_FILE	"/tmp/catt.status"

#define CMD_INFO	"catt info"
#define CMD_REWIND	"catt rewind"
#define CMD_FFWD	"catt ffwd"
#define CMD_PAUSE	"catt pause"
#define CMD_RESUME	"catt play"
#define CMD_STOP	"catt stop"
#define CMD_SEEK	"catt seek"
#define CMD_VOLUP	"catt volumeup"
#define CMD_VOLDOWN	"catt volumedown"
#define CMD_PLAY	"catt cast"
#define PARSE_PIPE	" | sed -n 's/Playing \"\\(.*\\)\" on \"[^\"]*\"...$/\\1/p'"

#define CMD_STATUS		"catt status > " STATUS_FILE " 2>&1"
#define CMD_PARSE_TITLE	"sed -n 's/Title:\\ \\(.*\\)$/\\1/p' " STATUS_FILE
#define CMD_PARSE_REM	"sed -n 's/Remaining\\ time:\\ \\(.*\\)$/\\1/p' " STATUS_FILE
#define CMD_PARSE_STATE	"sed -n 's/State:\\ \\(.*\\)$/\\1/p' " STATUS_FILE
#define CMD_PARSE_VOL	"sed -n 's/Volume:\\ \\(.*\\)$/\\1/p' " STATUS_FILE

#define CMD_PLAYING	"catt status | grep -iq paused"

FILE *fp;
GtkWidget *label;

static char cmd[64];
struct status {
	char title[256];
	int time_total_min;
	int time_elap_min;
	int time_rem_min;
	char state[16];
	int volume;
};

static int cmd_send(char *cmd_str)
{
	g_print ("%s\n", cmd);
	return system (cmd);
}

static int playing = 1;

static void callback_rw_sec (GtkWidget *widget, gpointer data)
{
	int sec = atoi(gtk_entry_get_text (GTK_ENTRY (data)));
	sprintf (cmd, CMD_REWIND " %d", sec);
	cmd_send(cmd);
}
static void callback_ff_sec (GtkWidget *widget, gpointer data)
{
	int sec = atoi(gtk_entry_get_text (GTK_ENTRY (data)));
	sprintf (cmd, CMD_FFWD " %d", sec);
	cmd_send(cmd);
}
static void callback_rw_min (GtkWidget *widget, gpointer data)
{
	int sec = 60 * atoi(gtk_entry_get_text (GTK_ENTRY (data)));
	sprintf (cmd, CMD_REWIND " %d", sec);
	cmd_send(cmd);
}
static void callback_ff_min (GtkWidget *widget, gpointer data)
{
	int sec = 60 * atoi(gtk_entry_get_text (GTK_ENTRY (data)));
	sprintf (cmd, CMD_FFWD " %d", sec);
	cmd_send(cmd);
}
static void callback_vol_up (GtkWidget *widget, gpointer data)
{
	int percent = atoi(gtk_entry_get_text (GTK_ENTRY (data)));
	sprintf (cmd, CMD_VOLUP " %d", percent);
	cmd_send(cmd);
}
static void callback_vol_down (GtkWidget *widget, gpointer data)
{
	int percent = atoi(gtk_entry_get_text (GTK_ENTRY (data)));
	sprintf (cmd, CMD_VOLDOWN " %d", percent);
	cmd_send(cmd);
}
static void callback_pause (GtkWidget *widget, gpointer data)
{
	if (playing)
		sprintf (cmd, CMD_PAUSE);
	else
		sprintf (cmd, CMD_RESUME);
	cmd_send(cmd);

	sprintf (cmd, CMD_PLAYING);
	playing = cmd_send(cmd);
}
static void callback_play (GtkWidget *widget, gpointer data)
{
	char title[256];
	char word[32];
	sprintf (cmd, CMD_PLAY " %s" PARSE_PIPE, gtk_entry_get_text (GTK_ENTRY (data)));

	fp = popen (cmd, "r"); 
	while (EOF != fscanf (fp, "%s", word)) {
		strcat (title, " ");
		strcat (title, word);
	}
	gtk_label_set_ellipsize (GTK_LABEL (label), PANGO_ELLIPSIZE_MIDDLE);
	gtk_label_set_text (GTK_LABEL (label), title);
	//printf ("=title= %s\n", title);
	fclose(fp);

	/*
	cmd_send(cmd);
	sleep (1);
	sprintf (cmd, CMD_STATUS);
	cmd_send(cmd);
	sprintf (cmd, CMD_PARSE_TITLE);
	cmd_send(cmd);
	*/
}
static struct status callback_status (GtkWidget *widget, gpointer data)
{
	struct status st;
	sprintf (cmd, CMD_STATUS);
	cmd_send(cmd);
	return st;
}


void my_css(void){
    GtkCssProvider *provider;
    GdkDisplay *display;
    GdkScreen *screen;

    provider = gtk_css_provider_new ();
    display = gdk_display_get_default ();
    screen = gdk_display_get_default_screen (display);
    gtk_style_context_add_provider_for_screen (screen, GTK_STYLE_PROVIDER (provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

    const gchar *css_file = g_strconcat (g_get_user_config_dir (), "/castctrl/castctrl.css", NULL);
    GError *error = 0;

	gtk_css_provider_load_from_path(provider, css_file, NULL);
    g_object_unref (provider);
}
static void activate (GtkApplication *app, gpointer user_data)
{
	GtkWidget *window;

	GtkWidget *btn_play;
	GtkWidget *btn_rw_sec;
	GtkWidget *btn_ff_sec;
	GtkWidget *btn_rw_min;
	GtkWidget *btn_ff_min;
	GtkWidget *btn_vol_up;
	GtkWidget *btn_vol_down;
	GtkWidget *btn_pause;

    //btn_play =   gtk_button_new_with_label ("►");
    //btn_play =   gtk_button_new ();
    btn_play =   gtk_button_new_from_icon_name ("media-playback-start", GTK_ICON_SIZE_BUTTON);
	gtk_widget_set_name(btn_play, "button_play");
    gtk_widget_set_size_request(btn_play, 5, 10);

    btn_rw_sec = gtk_button_new_with_label ("◄ sec");
    btn_ff_sec = gtk_button_new_with_label ("sec ►");
    btn_rw_min = gtk_button_new_with_label ("◄ min");
    btn_ff_min = gtk_button_new_with_label ("min ►");
	btn_vol_up = gtk_button_new_with_label ("vol ▲");
	btn_vol_down = gtk_button_new_with_label ("vol ▼");
    btn_pause = gtk_button_new_with_label ("pause ▐▐"); // ◘
	// ►
	GtkWidget *inp_addr;
	GtkWidget *inp_ff_sec;
	GtkWidget *inp_ff_min;
	GtkWidget *inp_rw_sec;
	GtkWidget *inp_rw_min;
	GtkWidget *inp_vol_up;
	GtkWidget *inp_vol_down;

	inp_addr = gtk_entry_new ();
	gtk_entry_set_text (GTK_ENTRY (inp_addr), "");
	gtk_entry_set_max_width_chars (GTK_ENTRY (inp_addr), 45);

	inp_ff_sec = gtk_entry_new ();
	gtk_entry_set_text (GTK_ENTRY (inp_ff_sec), "10");
	gtk_entry_set_width_chars (GTK_ENTRY (inp_ff_sec), 4);

	inp_ff_min = gtk_entry_new ();
	gtk_entry_set_text (GTK_ENTRY (inp_ff_min), "2");
	gtk_entry_set_width_chars (GTK_ENTRY (inp_ff_min), 4);

	inp_rw_sec = gtk_entry_new ();
	gtk_entry_set_text (GTK_ENTRY (inp_rw_sec), "10");
	gtk_entry_set_width_chars (GTK_ENTRY (inp_rw_sec), 4);

	inp_rw_min = gtk_entry_new ();
	gtk_entry_set_text (GTK_ENTRY (inp_rw_min), "2");
	gtk_entry_set_width_chars (GTK_ENTRY (inp_rw_min), 4);

	inp_vol_up = gtk_entry_new ();
	gtk_entry_set_text (GTK_ENTRY (inp_vol_up), "5");
	gtk_entry_set_width_chars (GTK_ENTRY (inp_vol_up), 4);

	inp_vol_down = gtk_entry_new ();
	gtk_entry_set_text (GTK_ENTRY (inp_vol_down), "5");
	gtk_entry_set_width_chars (GTK_ENTRY (inp_vol_down), 4);

	GtkWidget *hbox_row1;
	GtkWidget *hbox_row2;
	GtkWidget *hbox_row3;
	GtkWidget *hbox_row4;
	GtkWidget *hbox_row5;
	GtkWidget *hbox_row6;
	GtkWidget *vbox0;

	window = gtk_application_window_new (app);
	gtk_window_set_title (GTK_WINDOW (window), "Accounts");
	gtk_window_set_default_size (GTK_WINDOW (window), 300, 240);
	gtk_window_set_resizable (GTK_WINDOW(window), FALSE);

	hbox_row1 = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
	label = gtk_label_new ("");
	gtk_label_set_text (GTK_LABEL (label), "  video url:");
	gtk_container_add (GTK_CONTAINER (hbox_row1), label);

	hbox_row2 = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
	//gtk_button_box_set_layout (GTK_BUTTON_BOX (hbox_row2), GTK_BUTTONBOX_END);
	gtk_box_set_homogeneous (GTK_BOX (hbox_row2), false);
	gtk_box_set_spacing (GTK_BOX (hbox_row2), 0);

	hbox_row3 = gtk_button_box_new (GTK_ORIENTATION_HORIZONTAL);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (hbox_row3), GTK_BUTTONBOX_SPREAD);
	gtk_box_set_spacing (GTK_BOX (hbox_row3), 20);

	hbox_row4 = gtk_button_box_new (GTK_ORIENTATION_HORIZONTAL);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (hbox_row4), GTK_BUTTONBOX_SPREAD);
	gtk_box_set_spacing (GTK_BOX (hbox_row4), 20);

	hbox_row5 = gtk_button_box_new (GTK_ORIENTATION_HORIZONTAL);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (hbox_row5), GTK_BUTTONBOX_SPREAD);
	gtk_box_set_spacing (GTK_BOX (hbox_row5), 20);

	hbox_row6 = gtk_button_box_new (GTK_ORIENTATION_HORIZONTAL);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (hbox_row6), GTK_BUTTONBOX_SPREAD);
	gtk_box_set_spacing (GTK_BOX (hbox_row6), 20);

	vbox0 = gtk_button_box_new (GTK_ORIENTATION_VERTICAL);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (vbox0), GTK_BUTTONBOX_SPREAD);
	gtk_box_set_homogeneous (GTK_BOX (vbox0), 1);

	gtk_container_add (GTK_CONTAINER (window), vbox0);

	g_signal_connect (btn_rw_sec,   "clicked", G_CALLBACK (callback_rw_sec), inp_rw_sec);
	g_signal_connect (btn_ff_sec,   "clicked", G_CALLBACK (callback_ff_sec), inp_ff_sec);
	g_signal_connect (btn_rw_min,   "clicked", G_CALLBACK (callback_rw_min), inp_rw_min);
	g_signal_connect (btn_ff_min,   "clicked", G_CALLBACK (callback_ff_min), inp_ff_min);
	g_signal_connect (btn_vol_up,   "clicked", G_CALLBACK (callback_vol_up), inp_vol_up);
	g_signal_connect (btn_vol_down, "clicked", G_CALLBACK (callback_vol_down), inp_vol_down);
	g_signal_connect (btn_pause,    "clicked", G_CALLBACK (callback_pause), NULL);
	g_signal_connect (btn_play,     "clicked", G_CALLBACK (callback_play), inp_addr);

	//g_signal_connect_swapped (btn_rw_sec, "clicked", G_CALLBACK (gtk_widget_destroy), window);
	//gtk_container_add (GTK_CONTAINER (hbox_row2), inp_addr);

	gtk_box_pack_start (GTK_BOX(hbox_row2), inp_addr, true, false, 0); 
	gtk_box_pack_end (GTK_BOX(hbox_row2), btn_play, false, false, 0); 
	//gtk_container_add (GTK_CONTAINER (hbox_row2), btn_play);

	gtk_container_add (GTK_CONTAINER (hbox_row3), inp_rw_min);
	gtk_container_add (GTK_CONTAINER (hbox_row3), inp_ff_min);
	gtk_container_add (GTK_CONTAINER (hbox_row3), inp_vol_up);
	gtk_container_add (GTK_CONTAINER (hbox_row4), btn_rw_min);
	gtk_container_add (GTK_CONTAINER (hbox_row4), btn_ff_min);
	gtk_container_add (GTK_CONTAINER (hbox_row4), btn_vol_up);

	gtk_container_add (GTK_CONTAINER (hbox_row5), inp_rw_sec);
	gtk_container_add (GTK_CONTAINER (hbox_row5), inp_ff_sec);
	gtk_container_add (GTK_CONTAINER (hbox_row5), inp_vol_down);

	gtk_container_add (GTK_CONTAINER (hbox_row6), btn_rw_sec);
	gtk_container_add (GTK_CONTAINER (hbox_row6), btn_ff_sec);
	gtk_container_add (GTK_CONTAINER (hbox_row6), btn_vol_down);

	gtk_container_add (GTK_CONTAINER (vbox0), hbox_row1);
	gtk_container_add (GTK_CONTAINER (vbox0), hbox_row2);
	gtk_container_add (GTK_CONTAINER (vbox0), hbox_row3);
	gtk_container_add (GTK_CONTAINER (vbox0), hbox_row4);
	gtk_container_add (GTK_CONTAINER (vbox0), hbox_row5);
	gtk_container_add (GTK_CONTAINER (vbox0), hbox_row6);

	gtk_container_add (GTK_CONTAINER (vbox0), btn_pause);


	gtk_widget_show_all (window);
}

int main (int argc, char **argv)
{
	GtkApplication *app;
	int status;
	gtk_init(&argc, &argv);
	my_css();
	app = gtk_application_new ("chromcast2.wrapper", G_APPLICATION_FLAGS_NONE);
	g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
	status = g_application_run (G_APPLICATION (app), argc, argv);
	g_object_unref (app);

	return status;
}

